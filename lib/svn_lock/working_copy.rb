require 'fileutils'

module SvnLock

  class WorkingCopy
    attr_accessor :rootdir

    def initialize(url, working_copy_name)

      @url = url
      @working_copy_name = working_copy_name

      %x(svn checkout #{url} #{working_copy_name})

      @rootdir = Dir.new(working_copy_name)

    end

    def svn_add_needs_lock(filename)
      Dir.chdir(rootdir)
      %x(svn propset 'svn:needs-lock' true "#{filename}" 2>&1)
    end

    def lock(filename)
      Dir.chdir(rootdir)
      %x(svn lock "#{filename}")
    end

    def create_file(filename)
      Dir.chdir(rootdir)
      FileUtils.touch(filename)
      %x(svn add "#{filename}")
    end

    def svn_commit_file(filename)
      Dir.chdir(rootdir)
      %x(svn commit -m 'testing' "#{filename}" 2>&1)
    end

    # don't use files with spaces here
    def svn_commit_files(filenames)
      Dir.chdir(rootdir)
      files = filenames.join(' ')
      %x(svn commit -m 'testing' #{files} 2>&1)
    end

    def modify_file(filename, message)
      Dir.chdir(rootdir)
      FileUtils.chmod "u=rw", filename
      File.open(filename, "w") do |file|
        file.puts message
      end
    end

  end

end
