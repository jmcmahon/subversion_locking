require 'spec_helper'
require 'svn_lock/working_copy'
require 'fileutils'

module SvnLock

  describe WorkingCopy do

    pwd = File.dirname(__FILE__)
    
    before(:all) do
      
      # define folder instance variables
      @svnroot_dir = File.expand_path(File.join(pwd, 'fixtures/svnroot'))
      @subversion_folder = File.join(@svnroot_dir, "repo_w_locks")
      @subversion_hooks_folder = File.join(@subversion_folder, "hooks")
      @fixtures_hooks_folder = File.expand_path(File.join(pwd, 'fixtures/hooks'))
      
      # remove any existing subversion repository
      FileUtils.remove_dir(@subversion_folder) if
      File.directory?(@subversion_folder)
      
      # create subversion repository
      Dir.chdir(@svnroot_dir)
      output = `svnadmin create repo_w_locks`
      # copy hooks to the repository hooks folder
      Dir.chdir(@fixtures_hooks_folder)
      FileUtils.cp_r Dir.glob("*"), @subversion_hooks_folder
      
      # define working copy folder name
      @working_copy_dir =
      File.expand_path(File.join(pwd,'fixtures/working_copy'))
      @working_copy_name = File.expand_path(File.join(@working_copy_dir,
      "repo_w_locks"))
      
      # remove any existing working copy folder
      FileUtils.remove_dir(@working_copy_name) if
      File.directory?(@working_copy_name)
      
      @url = "file://" + @subversion_folder

    end

    context "subversion locking" do

      let (:working_copy) { @working_copy = SvnLock::WorkingCopy.new(@url, @working_copy_name) }
      subject { working_copy }

	    context 'adding new files' do
	
	      it "should fail with svn:needs-lock error on commit if property svn:needs-lock is not set" do
	        working_copy.create_file("missing_needs_lock.txt")
	        output = working_copy.svn_commit_file("missing_needs_lock.txt")
	        output.should match(/svn:needs-lock/)
	      end
	      
	      it "should commit file if svn:needs-lock property is set" do
	        working_copy.create_file("has_needs_lock.txt")
	        working_copy.svn_add_needs_lock("has_needs_lock.txt")
	        output = working_copy.svn_commit_file("has_needs_lock.txt")
	        output.should match(/Committed revision/)
	      end
	
	    end
	    
	    context 'updating existing files' do
	
	      it "should fail to commit if a lock is not set" do
	        working_copy.create_file("lock_not_set.txt")
	        working_copy.svn_add_needs_lock("lock_not_set.txt")
	        output = working_copy.svn_commit_file("lock_not_set.txt")
	        working_copy.modify_file("lock_not_set.txt", "this is a test")
	        output = working_copy.svn_commit_file("lock_not_set.txt")
	        output.should match(/File not locked/)
	      end
	      
	      it "should commit file if lock is set " do
	        working_copy.create_file("lock_was_set.txt")
	        working_copy.svn_add_needs_lock("lock_was_set.txt")
	        output = working_copy.svn_commit_file("lock_was_set.txt")
	        working_copy.modify_file("lock_was_set.txt", "this is a test")
	        working_copy.lock("lock_was_set.txt")
	        output = working_copy.svn_commit_file("lock_was_set.txt")
	        output.should match(/Committed revision/)
	      end
	
	    end

	    context 'multiple errors'  do
	
	      it "should have an error message for each file in error" do
	        working_copy.create_file("multi_1.txt")
	        working_copy.svn_add_needs_lock("multi_1.txt")
	        output = working_copy.svn_commit_file("multi_1.txt")
	        working_copy.modify_file("multi_1.txt", "this is a test")

	        working_copy.create_file("multi_2.txt")

	        output = working_copy.svn_commit_files(["multi_1.txt", "multi_2.txt"])
	        output.should match(/File not locked/)
	        output.should match(/svn:needs-lock/)
	      end
	
	    end

	    context 'file with 1 character'  do
	
	      it "should be able to commit a file with a 1-character name" do
	        working_copy.create_file("a")
	        working_copy.svn_add_needs_lock("a")
	        output = working_copy.svn_commit_file("a")
	        output.should match(/Committed revision/)
	      end
	
	    end

	    context 'file with spaces'  do
	
	      it "should be able to commit a file with spaces in the name" do
	        working_copy.create_file("this\ is\ a\ file\ with\ spaces\ in\ the\ name.txt")
	        working_copy.svn_add_needs_lock("this\ is\ a\ file\ with\ spaces\ in\ the\ name.txt")
	        output = working_copy.svn_commit_file("this\ is\ a\ file\ with\ spaces\ in\ the\ name.txt")
	        output.should match(/Committed revision/)
          puts output
	      end
	
	    end

    end

  end

end
