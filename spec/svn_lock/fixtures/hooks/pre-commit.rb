#!/usr/bin/env ruby

repository = ARGV[0]
transaction = ARGV[1]

# shouldn't need this with newer versions of Ruby.
# added this in case you are stuck with an ancient 
# version of ruby like 1.8.1
class String
  unless "".respond_to?(:end_with?)
    def end_with?(value)
      self[-1].chr == value
    end
  end
end

class Item

  attr_reader :item, :repository, :transaction

  def initialize(item, repository, transaction)
    @item = item
    @repository = repository
    @transaction = transaction
  end

  def directory?
    item.end_with?("/")
  end

  def file?
    item.end_with?("/") == false
  end

  def needs_lock?
    needs_lock = %x(svnlook propget "#{repository}" svn:needs-lock "#{item}" --transaction "#{transaction}" 2>/dev/null)
    needs_lock.length > 0
  end

  def locked?
    lock = %x(svnlook lock #{repository} #{item})
    lock.length > 0
  end

  def to_s
    "#{item}"
  end

end

class Change
  attr_reader :action, :item
  def initialize(action, item, repository, transaction)
    @action = action
    @item = Item.new(item, repository, transaction )
  end

  def update?
    action == "U"
  end

  def add?
    action == "A"
  end

  def delete?
    action == "D"
  end

  def to_s
    "action = #{action} file = #{item}"
  end

end

def check_changes(pre_commit_changes)

  errors = false

  pre_commit_changes.each do |change|
    (warn("File not locked: #{change.item}");  errors = true ) if change.item.file? and change.item.needs_lock? and not change.item.locked? and change.update?
    (warn("Missing svn:needs-lock property: #{change.item}"); errors = true ) if change.item.file? and change.add? and not change.item.needs_lock?
  end

  exit 1 if errors == true

end

pre_commit_changes = []

changes = `svnlook changed #{repository} --transaction #{transaction}`
changes.each_line do |change|
  action, item = /(\S)\s+(\S.*)$/.match(change).captures
  item.strip!
  pre_commit_changes << Change.new(action, item, repository, transaction )
end

check_changes(pre_commit_changes)
